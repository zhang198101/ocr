package com.handy.ocr.vo.accurategeneral;

import lombok.Data;

import java.io.Serializable;

/**
 * @author hanshuai
 * @Description: {坐标}
 * @date 2019/9/10
 */
@Data
public class VertexesLocation implements Serializable {
    private int x;
    private int y;
}
