package com.handy.ocr.service;

import com.handy.ocr.OcrApplicationTests;
import com.handy.ocr.util.PoiToWord;
import lombok.val;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author hs
 * @Description: {}
 * @date 2019/9/10 10:08
 */
public class BaiDuOcrServiceTest extends OcrApplicationTests {
    @Autowired
    private IBaiDuOcrService baiDuOcrService;

    /**
     * 营业执照识别
     */
    @Test
    public void businessLicense() {
        String url = "c:/java/营业执照.png";
        val businessLicenseVo = baiDuOcrService.businessLicense(url, null);
        System.out.println(businessLicenseVo.toString());
    }

    /**
     * 身份证正面识别
     */
    @Test
    public void idCardFront() {
        String url = "c:/java/身份证正面.png";
        val idCardFrontVo = baiDuOcrService.idCardFront(url, null);
        System.out.println(idCardFrontVo.toString());
    }

    /**
     * 身份证背面识别
     */
    @Test
    public void idCardBack() {
        String url = "c:/java/身份证背面.png";
        val idCardBackVo = baiDuOcrService.idCardBack(url, null);
        System.out.println(idCardBackVo.toString());
    }

    /**
     * 通用文字识别(含位置高精度版本)
     */
    @Test
    public void accurateGeneral() throws Exception {
        String url = "c:/java/文字.png";
        val accurateGeneralVo = baiDuOcrService.accurateGeneral(url, null);
        System.out.println(accurateGeneralVo.toString());
        // poi生成word
        PoiToWord.poiToWord(accurateGeneralVo, url);
    }
}
