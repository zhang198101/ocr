# 百度ocr

#### 介绍
使用百度ocr技术来识别文字的demo,内容有识别通用文字,识别身份证和营业执照
并可以使用poi来将识别到的图片转换为跟图片一样的docx文件  
如果本项目帮助到您,请点个Star,就是对开发者最大的支持了,谢谢 

# 效果图

- 原图
![原图](https://images.gitee.com/uploads/images/2019/0910/105428_00440b2a_1604115.png "文字.png")

- 转换后的docx 
![转换后的docx](https://images.gitee.com/uploads/images/2019/0910/105508_ee801764_1604115.jpeg "1568084094(1).jpg")
